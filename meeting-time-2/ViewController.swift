
//
//  ViewController.swift
//  meeting-time-2
//
//  Created by Simon Knittel on 19.04.16.
//  Copyright © 2016 hmmh multimediahaus AG. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    // Collection View Cell Identifiers
    let SCRUMDailyCellIdentifier = "SCRUMDailyCell"
    let MeetingCellIdentifier = "MeetingCell"
    let PresentationCellIdentifier = "PresentationCell"
    
    // Collection View Outlets
    @IBOutlet weak var SCRUMDailyCollectionView: UICollectionView!
    @IBOutlet weak var MeetingCollectionView: UICollectionView!
    @IBOutlet weak var PresentationCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        SCRUMDailyCollectionView.delegate = self
        MeetingCollectionView.delegate = self
        PresentationCollectionView.delegate = self
        
        SCRUMDailyCollectionView.dataSource = self
        MeetingCollectionView.dataSource = self
        PresentationCollectionView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.SCRUMDailyCollectionView!.reloadData()
        self.MeetingCollectionView!.reloadData()
        self.PresentationCollectionView!.reloadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SCRUMDailyPresettings" {
            if let destination = segue.destinationViewController as? DAILYScrumSettingsViewController {

                if let indexPath = self.SCRUMDailyCollectionView?.indexPathForCell(sender as! UICollectionViewCell) {
                    destination.customTitle = SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].title
                    destination.participants = SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].participants
                    destination.duration = SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].duration
                    destination.animation = SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].animation
                    destination.order = SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].order
                }

            }
        } else if segue.identifier == "MeetingPresettings" {
            if let destination = segue.destinationViewController as? MeetingSettingsViewController {

                if let indexPath = self.MeetingCollectionView?.indexPathForCell(sender as! UICollectionViewCell) {
                    destination.customTitle = SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].title
                    destination.duration = SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].duration
                    destination.animation = SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].animation
                    destination.topics = SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].topics
                    destination.order = SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].order
                }
                
            }
        } else if segue.identifier == "PresentationPresettings" {

        }
    }
    
    // Amount of Collection View Cells
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.SCRUMDailyCollectionView {
            return SharingManager.sharedInstance.SCRUMDailyItems.count
        } else if collectionView == self.MeetingCollectionView {
            return SharingManager.sharedInstance.MeetingItems.count
        } else if collectionView == self.PresentationCollectionView {
            return SharingManager.sharedInstance.PresentationItems.count
        }

        return 0
    }

    // Set up each Collection View Cell
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if collectionView == self.SCRUMDailyCollectionView {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(SCRUMDailyCellIdentifier, forIndexPath: indexPath) as! MyCollectionViewCell // Get the Cell
            
            // Set up the Cell
            cell.label.text = SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.SCRUMDailyItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].title
            
            return cell // Return the Cell
        } else if collectionView == self.MeetingCollectionView {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(MeetingCellIdentifier, forIndexPath: indexPath) as! MyCollectionViewCell // Get the Cell
            
            // Set up the Cell
            cell.label.text = SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.MeetingItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].title
            
            return cell // Return the Cell
        } else {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(PresentationCellIdentifier, forIndexPath: indexPath) as! MyCollectionViewCell // Get the Cell
            
            // Set up the Cell
            cell.label.text = SharingManager.sharedInstance.PresentationItems.sort({$0.order < $1.order})[SharingManager.sharedInstance.PresentationItems.sort({$0.order < $1.order}).startIndex.advancedBy(indexPath.item)].title
            
            return cell // Return the Cell
        }
    }
    
    // Tap on Collection View Cell
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {}
}

