//
//  ViewViewController.swift
//  meeting-time-2
//
//  Created by Simon Knittel on 21.04.16.
//  Copyright © 2016 hmmh multimediahaus AG. All rights reserved.
//

import UIKit

class DAILYScrumViewViewController: UIViewController {
    var customTitle: String!
    var participants: Int!
    var duration: Int!
    var animation: Int!
    var timer = NSTimer()
    var remainingSeconds: Int = 0;
    var currentParticipantNumber: Int = 1 // Start with first participant
    var currentTopic: Int = 1 // Start with first topic
    var topics: Array = ["Was habe ich gemacht?", "Was mache ich als nächstes?", "Was blockiert mich?"]
    var phase: Int = 1 // 0 = Pausiert, 1 = Title screen, 2 = Topic screen, 3 = Last topic, 4 = Participant screen, 5 = Last screen
    
    @IBOutlet weak var participantNumberLabel: UILabel!
    @IBOutlet weak var topicLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.topicLabel.text = self.customTitle
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

        // Dispose of any resources that can be recreated.
    }
    
    func fullcolorAnimation() {
        let hue:CGFloat = 120 / CGFloat(self.duration / self.participants) * CGFloat(self.remainingSeconds) / 360
        UIView.animateWithDuration(1,
            delay: 0.0,
            options: .CurveLinear,
            animations: { () -> Void in
                self.view.backgroundColor = UIColor(hue: hue, saturation: 1.0, brightness: 1.0, alpha: 1)
            },
            completion: nil
        )
    }
    
    func pulsAnimation() {
        UIView.animateWithDuration(1,
            delay: 0.0,
            options: .CurveEaseInOut,
            animations: {
                self.view.transform = CGAffineTransformScale(
                self.view.transform, CGFloat(2), CGFloat(2))
            },
            completion: { _ in
                UIView.animateWithDuration(1,
                    delay: 0.0,
                    options: .CurveEaseInOut,
                    animations: {
                        self.view.transform = CGAffineTransformScale(
                        self.view.transform, CGFloat(-2), CGFloat(-2))
                    },
                    completion: { _ in
                        self.pulsAnimation()
                    }
                )
            }
        )
    }

    func timerInterval() {
        self.remainingSeconds -= 1

        if self.animation == 1 {
            self.pulsAnimation()
        } else {
            self.fullcolorAnimation()
        }
    }
    
    func startCountdown() {
        self.timer.invalidate()

        self.remainingSeconds = self.duration / self.participants
        
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(DAILYScrumViewViewController.timerInterval), userInfo: nil, repeats: true)
        
        if self.animation == 1 {
            self.pulsAnimation()
        } else {
            self.fullcolorAnimation()
        }
    }

    func resetCountdown() {
        self.timer.invalidate()

        self.remainingSeconds = self.duration / self.participants

        if self.animation == 1 {
            self.pulsAnimation()
        } else {
            self.fullcolorAnimation()
        }
    }

    @IBAction func continueAction(sender: UIButton) {
        if self.phase == 1 { // Switch to phase 2
            self.phase = 2

            self.topicLabel.text = topics[self.currentTopic - 1]
            self.participantNumberLabel.text = String(self.currentParticipantNumber)

            self.continueButton.setTitle("Nächstes Thema", forState: .Normal)

            self.startCountdown()
        } else if self.phase == 2 { // Switch to next topic
            self.currentTopic += 1

            self.topicLabel.text = self.topics[self.currentTopic - 1]
            self.participantNumberLabel.text = String(self.currentParticipantNumber)
            
            if self.currentTopic == self.topics.count {
                if self.currentParticipantNumber == self.participants { // Switch to phase 4
                    self.continueButton.setTitle("Beenden", forState: .Normal)
                    
                    self.phase = 5
                } else { // Switch to phase 3
                    self.continueButton.setTitle("Nächster Teilnehmer", forState: .Normal)
                    
                    self.phase = 3
                }
            } else {
                self.continueButton.setTitle("Nächstes Thema", forState: .Normal)
            }
        } else if self.phase == 3 {
            self.phase = 4

            self.currentParticipantNumber += 1
            self.participantNumberLabel.text = String(self.currentParticipantNumber)

            self.topicLabel.text = "Teilnehmer " + String(self.currentParticipantNumber)
            self.continueButton.setTitle("Start", forState: .Normal)

            self.resetCountdown()
        } else if self.phase == 4 { // Switch to phase 2
            self.phase = 2
            self.currentTopic = 1
            
            self.topicLabel.text = self.topics[self.currentTopic - 1]
            
            self.continueButton.setTitle("Nächstes Thema", forState: .Normal)
            
            self.startCountdown()
        } else if self.phase == 5 { // Show Evaluation View Controller
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EvaluationID") as! EvaluationViewController
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
}