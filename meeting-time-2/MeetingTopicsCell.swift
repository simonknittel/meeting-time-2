//
//  MeetingTopicsCell.swift
//  meeting-time-2
//
//  Created by Simon Knittel on 29.04.16.
//  Copyright © 2016 hmmh multimediahaus AG. All rights reserved.
//

import UIKit

class MeetingTopicsCell: UITableViewCell {
    @IBOutlet weak var textField: UITextField!

    override func canBecomeFocused() -> Bool {
        return false
    }
}
