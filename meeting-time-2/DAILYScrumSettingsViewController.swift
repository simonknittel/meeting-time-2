//
//  SettingsViewController.swift
//  meeting-time-2
//
//  Created by Simon Knittel on 20.04.16.
//  Copyright © 2016 hmmh multimediahaus AG. All rights reserved.
//

import UIKit

class DAILYScrumSettingsViewController: UIViewController {
    var customTitle: String!
    var participants: Int!
    var duration: Int!
    var animation: Int!
    var order: Int!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var participantsCountLabel: UILabel!
    @IBOutlet weak var participantsTimeMinutesLabel: UILabel!
    @IBOutlet weak var participantsTimeSecondsLabel: UILabel!
    @IBOutlet weak var animationSelection: UISegmentedControl!
    @IBOutlet weak var removeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.displayValues()
        self.animationSelection.selectedSegmentIndex = animation
        self.titleLabel.text = self.customTitle

        if self.order != 0 && self.order != 99999 {
            self.removeButton.hidden = false // Show button
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    // Data transfer to the next ViewController
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SCRUMDailySettings" {
            if let destination = segue.destinationViewController as? DAILYScrumViewViewController {
                destination.customTitle = self.customTitle
                destination.participants = self.participants
                destination.duration = self.duration
                destination.animation = self.animationSelection.selectedSegmentIndex
            }
        }
    }
    
    func displayValues() {
        self.participantsCountLabel.text = String(self.participants) // Display participants count
        
        // Display time for each participant
        self.participantsTimeMinutesLabel.text = String(self.duration / 60) // Display minutes
        self.participantsTimeSecondsLabel.text = String(format: "%02d", self.duration % 60) // Display seconds
    }
    
    // Participants count actions
    @IBAction func btnIncreaseParticipants(sender: UIButton) {
        self.participants = self.participants + 1 // Add one participant
        self.displayValues()
    }
    @IBAction func btnDecreaseParticipants(sender: UIButton) {
        if self.participants > 2 {
            self.participants = self.participants - 1 // Remove one participant
        }
        self.displayValues()
    }
    
    // Participant time actions
    @IBAction func btnIncreaseParticipantTimeMinutes(sender: UIButton) {
        self.duration = self.duration + 60 // Add 1 minute / 60 seconds
        self.displayValues()
    }
    @IBAction func btnDecreaseParticipantTimeMinutes(sender: UIButton) {
        self.duration = self.duration - 60 // Remove 1 minute / 60 seconds
        if self.duration <= 0 {
            self.duration = self.duration + 60 // Add 1 minute / 60 seconds
        }
        self.displayValues()
    }
    @IBAction func btnIncreaseParticipantTimeSeconds(sender: UIButton) {
        self.duration = self.duration + 10 // Add 10 seconds
        self.displayValues()
    }
    @IBAction func btnDecreaseParticipantTimeSeconds(sender: UIButton) {
        self.duration = self.duration - 10 // Remove 10 seconds
        if self.duration <= 0 {
            self.duration = self.duration + 10 // Add 10 seconds
        }
        self.displayValues()
    }
    
    func enterTitle(reenter: Bool = false) {
        var message: String = ""
        if reenter {
            message = "Dieser Name kann nicht verwendet werden."
        }
        let alertController = UIAlertController(title: "Titel vergeben", message: message, preferredStyle: .Alert) // Create a AlertController
        
        // Adds a text field to the alert
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Titel"
            textField.keyboardType = .Default
            
            if self.order != 0 && self.order != 99999 {
                textField.text = self.customTitle
            }
        }
        
        // What to do when the user pressed "Abbrechen"
        let cancelAction = UIAlertAction(title: "Abbrechen", style: .Cancel) { (action) in }
        alertController.addAction(cancelAction)
        
        // What to do when the user pressed "Speichern"
        let OKAction = UIAlertAction(title: "Speichern", style: .Default) { (action) in
            let textField = alertController.textFields![0] as UITextField
            
            if textField.text?.lowercaseString == "SCRUM Daily".lowercaseString || textField.text?.lowercaseString == "Meeting" || textField.text?.lowercaseString == "Präsentation".lowercaseString || textField.text?.lowercaseString == "Neu".lowercaseString {
                self.enterTitle(true)
            }

            if !SharingManager.sharedInstance.addSCRUMDaily(textField.text!, participants: self.participants, duration: self.duration, animation: self.animationSelection.selectedSegmentIndex) {
                 // Do something when item could not be saved
            } else {
                // Do something when item could be saved
                self.removeButton.hidden = false
            }
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true) {} // Show the AlertController
    }

    @IBAction func saving(sender: UIButton) {
        enterTitle()
    }

    @IBAction func remove(sender: UIButton) {
        if SharingManager.sharedInstance.removeSCRUMDailyItem(self.customTitle) {
            self.removeButton.hidden = true
        }
    }
}
