//
//  ViewViewController.swift
//  meeting-time-2
//
//  Created by Simon Knittel on 21.04.16.
//  Copyright © 2016 hmmh multimediahaus AG. All rights reserved.
//

import UIKit

class MeetingViewViewController: UIViewController {
    var customTitle: String!
    var duration: Int!
    var animation: Int!
    var remainingSeconds: Int = 0;
    var currentTopic: Int = 1 // Start with first topic
    var topics: Array<String>!

    @IBOutlet weak var topicLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.topicLabel.text = self.customTitle
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

        // Dispose of any resources that can be recreated.
    }

    @IBAction func continueAction(sender: UIButton) {}
}