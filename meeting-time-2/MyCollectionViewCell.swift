//
//  MyCollectionViewCell.swift
//  meeting-time-2
//
//  Created by Simon Knittel on 19.04.16.
//  Copyright © 2016 hmmh multimediahaus AG. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var label: UILabel!

//    Do stuff on cell focus (Example: Add a red border)
//    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
//        coordinator.addCoordinatedAnimations({
//            if self.focused {
//                self.layer.borderWidth = 2
//                self.layer.borderColor = UIColor.redColor().CGColor
//            } else {
//                self.layer.borderWidth = 0
//            }
//        }, completion: nil)
//    }
}
