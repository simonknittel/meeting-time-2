//
//  SharingManager.swift
//  meeting-time-2
//
//  Created by Simon Knittel on 25.04.16.
//  Copyright © 2016 hmmh multimediahaus AG. All rights reserved.
//

// Collection View Items
struct SCRUMDailyItem: Equatable, Hashable {
    var title: String
    var participants: Int
    var duration: Int
    var animation: Int
    var order: Int
    
    init(title: String, participants: Int, duration: Int, animation: Int, order: Int = 1) {
        self.title = title
        self.participants = participants
        self.duration = duration
        self.animation = animation
        self.order = order
    }

    // Add hashable
    var hashValue: Int {
        return self.title.hashValue << 15 + self.participants.hashValue + self.duration.hashValue + self.animation.hashValue
    }
}
// Add equatable
func ==(lhs: SCRUMDailyItem, rhs: SCRUMDailyItem) -> Bool {
    return lhs.title == rhs.title
        && lhs.participants == rhs.participants
        && lhs.duration == rhs.duration
        && lhs.animation == rhs.animation
}

struct MeetingItem: Equatable, Hashable {
    var title: String
    var duration: Int
    var animation: Int
    var order: Int
    var topics: Array<String>

    init(title: String, duration: Int, animation: Int, topics: Array<String>, order: Int = 1) {
        self.title = title
        self.duration = duration
        self.animation = animation
        self.order = order
        self.topics = topics
    }

    // Add hashable
    var hashValue: Int {
//        return self.title.hashValue << 15 + self.duration.hashValue + self.animation.hashValue + self.topics.joinWithSeparator(", ").hashValue
        return self.title.hashValue << 15 + self.duration.hashValue + self.animation.hashValue
    }
}
// Add equatable
func ==(lhs: MeetingItem, rhs: MeetingItem) -> Bool {
    return lhs.title == rhs.title
        && lhs.duration == rhs.duration
        && lhs.animation == rhs.animation
        && lhs.topics == rhs.topics
}

struct PresentationItem: Equatable, Hashable {
    var title: String
    var duration: Int
    var animation: Int
    var order: Int

    init(title: String, duration: Int, animation: Int, order: Int = 1) {
        self.title = title
        self.duration = duration
        self.animation = animation
        self.order = order
    }

    // Add hashable
    var hashValue: Int {
        return self.title.hashValue << 15 + self.duration.hashValue + self.animation.hashValue
    }
}
// Add equatable
func ==(lhs: PresentationItem, rhs: PresentationItem) -> Bool {
    return lhs.title == rhs.title
        && lhs.duration == rhs.duration
        && lhs.animation == rhs.animation
}

class SharingManager {
    var SCRUMDailyItems = Set<SCRUMDailyItem>()
    var MeetingItems = Set<MeetingItem>()
    var PresentationItems = Set<PresentationItem>()
    
    init() {
        // Add default SCRUM Daily items
        self.SCRUMDailyItems.insert(SCRUMDailyItem(title: "SCRUM Daily", participants: 3, duration: 540, animation: 0, order: 0))
        self.SCRUMDailyItems.insert(SCRUMDailyItem(title: "Eddie Bauer Daily", participants: 4, duration: 720, animation: 0))
        self.SCRUMDailyItems.insert(SCRUMDailyItem(title: "Neu", participants: 3, duration: 540, animation: 0, order: 99999))

        // Add default Meeting items
        self.MeetingItems.insert(MeetingItem(title: "Meeting", duration: 2000, animation: 0, topics: ["test 1", "test 2"], order: 0))
        self.MeetingItems.insert(MeetingItem(title: "CoP Frontend", duration: 1500, animation: 0, topics: []))
        self.MeetingItems.insert(MeetingItem(title: "ALDI SÜD Kick-off", duration: 720, animation: 0, topics: []))
        self.MeetingItems.insert(MeetingItem(title: "Neu", duration: 500, animation: 0, topics: [], order: 99999))

        // Add default Presentation items
        self.PresentationItems.insert(PresentationItem(title: "Präsentation", duration: 1000, animation: 0, order: 0))
        self.PresentationItems.insert(PresentationItem(title: "ALDI SÜD Pitch", duration: 500, animation: 0))
        self.PresentationItems.insert(PresentationItem(title: "Neu", duration: 200, animation: 0, order: 99999))
    }

    // Add SCRUM Daily item (overrides items with same title)
    func addSCRUMDaily(title: String, participants: Int, duration: Int, animation: Int) -> Bool {
        self.removeSCRUMDailyItem(title) // Remove existing item

        
        self.SCRUMDailyItems.insert(SCRUMDailyItem(title: title, participants: participants, duration: duration, animation: animation)) // Insert new item
        return true
    }

    // Add Meeting item
    func addMeetingItem(title: String, duration: Int, animation: Int, topics: Array<String>) -> Bool {
        self.removeMeetingItem(title) // Remove existing item

        self.MeetingItems.insert(MeetingItem(title: title, duration: duration, animation: animation, topics: topics)) // Insert new item
        return true
    }

    // Add Presentation item
    func addPresentationItem(title: String, duration: Int, animation: Int) -> Bool {
        self.removePresentationItem(title) // Remove existing item

        self.PresentationItems.insert(PresentationItem(title: title, duration: duration, animation: animation)) // Insert new item
        return true
    }

    // Remove SCRUM Daily item
    func removeSCRUMDailyItem(title: String) -> Bool {
        var removed: Bool = false

//        loop: for item in self.SCRUMDailyItems {
//            if item.title == title {
//                self.SCRUMDailyItems.remove(SCRUMDailyItem(title: item.title, participants: item.participants, duration: item.duration, animation: item.animation, order: item.order)) // Remove item
//                removed = true
//                break loop
//            }
//        }

        let itemsToRemove: Array<SCRUMDailyItem> = SCRUMDailyItems.filter({ $0.title == title }) // Find item
        if !itemsToRemove.isEmpty {
            let itemToRemove = itemsToRemove.first
            self.SCRUMDailyItems.remove(SCRUMDailyItem(title: itemToRemove!.title, participants: itemToRemove!.participants, duration: itemToRemove!.duration, animation: itemToRemove!.animation, order: itemToRemove!.order)) // Remove item
            removed = true
        }

        return removed
    }

    // Remove Meeting item
    func removeMeetingItem(title: String) -> Bool {
        var removed: Bool = false

//        loop: for item in self.MeetingItems {
//            if item.title == title {
//                self.MeetingItems.remove(MeetingItem(title: item.title, duration: item.duration, animation: item.animation, topics: item.topics, order: item.order)) // Remove item
//                removed = true
//                break loop
//            }
//        }

        let itemsToRemove: Array<MeetingItem> = MeetingItems.filter({ $0.title == title }) // Find item
        if !itemsToRemove.isEmpty {
            let itemToRemove = itemsToRemove.first
            self.MeetingItems.remove(MeetingItem(title: itemToRemove!.title, duration: itemToRemove!.duration, animation: itemToRemove!.animation, topics: itemToRemove!.topics, order: itemToRemove!.order)) // Remove item
            removed = true
        }

        return removed
    }

    // Remove Presentation item
    func removePresentationItem(title: String) -> Bool {
        var removed: Bool = false

//        loop: for item in self.PresentationItems {
//            if item.title == title {
//                self.PresentationItems.remove(PresentationItem(title: item.title, duration: item.duration, animation: item.animation, order: item.order)) // Remove item
//                removed = true
//                break loop
//            }
//        }

        let itemsToRemove: Array<PresentationItem> = PresentationItems.filter({ $0.title == title }) // Find item
        if !itemsToRemove.isEmpty {
            let itemToRemove = itemsToRemove.first
            self.PresentationItems.remove(PresentationItem(title: itemToRemove!.title, duration: itemToRemove!.duration, animation: itemToRemove!.animation, order: itemToRemove!.order)) // Remove item
            removed = true
        }

        return removed
    }

    static let sharedInstance = SharingManager()
}