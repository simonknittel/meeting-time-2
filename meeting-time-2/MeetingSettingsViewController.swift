//
//  MeetingSettingsViewController.swift
//  meeting-time-2
//
//  Created by Simon Knittel on 28.04.16.
//  Copyright © 2016 hmmh multimediahaus AG. All rights reserved.
//

import UIKit

class MeetingSettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var customTitle: String!
    var duration: Int!
    var animation: Int!
    var order: Int!
    var topics: Array<String>!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var durationMinutesLabel: UILabel!
    @IBOutlet weak var durationSecondsLabel: UILabel!
    @IBOutlet weak var animationSelection: UISegmentedControl!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var topicsTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.displayValues()
        self.animationSelection.selectedSegmentIndex = animation
        self.titleLabel.text = self.customTitle

        if self.order != 0 && self.order != 99999 {
            self.removeButton.hidden = false // Show button
        }

        topicsTable.delegate = self
        topicsTable.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

        // Dispose of any resources that can be recreated.
    }

    // Data transfer to the next ViewController
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "MeetingSettings" {
            if let destination = segue.destinationViewController as? MeetingViewViewController {
                destination.customTitle = self.customTitle
                destination.duration = self.duration
                destination.animation = self.animationSelection.selectedSegmentIndex
                destination.topics = self.topics
            }
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topics.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = topicsTable.dequeueReusableCellWithIdentifier("MeetingTopicsCell", forIndexPath: indexPath) as! MeetingTopicsCell

        cell.textField.placeholder = "Topic"
        cell.textField.keyboardType = .Default
        cell.textField.returnKeyType = .Done
        cell.textField.text = topics[indexPath.row]

        return cell
    }

    func displayValues() {
        // Display duration
        self.durationMinutesLabel.text = String(self.duration / 60) // Display minutes
        self.durationSecondsLabel.text = String(format: "%02d", self.duration % 60) // Display seconds
    }

    // Duration actions
    @IBAction func btnIncreaseDurationMinutes(sender: UIButton) {
        self.duration = self.duration + 60 // Add 1 minute / 60 seconds
        self.displayValues()
    }
    @IBAction func btnDecreaseDurationeMinutes(sender: UIButton) {
        self.duration = self.duration - 60 // Remove 1 minute / 60 seconds
        if self.duration <= 0 {
            self.duration = self.duration + 60 // Add 1 minute / 60 seconds
        }
        self.displayValues()
    }
    @IBAction func btnIncreaseDurationSeconds(sender: UIButton) {
        self.duration = self.duration + 10 // Add 10 seconds
        self.displayValues()
    }
    @IBAction func btnDecreaseDurationSeconds(sender: UIButton) {
        self.duration = self.duration - 10 // Remove 10 seconds
        if self.duration <= 0 {
            self.duration = self.duration + 10 // Add 10 seconds
        }
        self.displayValues()
    }

    func enterTitle(reenter: Bool = false) {
        var message: String = ""
        if reenter {
            message = "Dieser Name kann nicht verwendet werden."
        }
        let alertController = UIAlertController(title: "Titel vergeben", message: message, preferredStyle: .Alert) // Create a AlertController

        // Adds a text field to the alert
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Titel"
            textField.keyboardType = .Default

            if self.order != 0 && self.order != 99999 {
                textField.text = self.customTitle
            }
        }

        // What to do when the user pressed "Abbrechen"
        let cancelAction = UIAlertAction(title: "Abbrechen", style: .Cancel) { (action) in }
        alertController.addAction(cancelAction)

        // What to do when the user pressed "Speichern"
        let OKAction = UIAlertAction(title: "Speichern", style: .Default) { (action) in
            let textField = alertController.textFields![0] as UITextField

            if textField.text?.lowercaseString == "SCRUM Daily".lowercaseString || textField.text?.lowercaseString == "Meeting" || textField.text?.lowercaseString == "Präsentation".lowercaseString || textField.text?.lowercaseString == "Neu".lowercaseString {
                self.enterTitle(true)
            }

            if !SharingManager.sharedInstance.addMeetingItem(textField.text!, duration: self.duration, animation: self.animationSelection.selectedSegmentIndex, topics: self.topics) {
                // Do something when item could not be saved
            } else {
                // Do something when item could be saved
                self.removeButton.hidden = false
            }
        }
        alertController.addAction(OKAction)

        self.presentViewController(alertController, animated: true) {} // Show the AlertController
    }

    @IBAction func saving(sender: UIButton) {
        enterTitle()
    }

    @IBAction func remove(sender: UIButton) {
        if SharingManager.sharedInstance.removeMeetingItem(self.customTitle) {
            self.removeButton.hidden = true
        }
    }
}
